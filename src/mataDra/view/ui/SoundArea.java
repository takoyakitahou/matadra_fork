package mataDra.view.ui;

import mataDra.logics.ui.MessageLogic;
import mataDra.logics.ui.StrongFrameLogic;
import mataDra.logics.ui.WaitLogic;

public class SoundArea {
	public void show(String text, int waitNum ){
		 //メッセージ用インスタンス
        MessageLogic messageLogic = new MessageLogic();
        // 外枠用インスタンス
        StrongFrameLogic frameLogic = new StrongFrameLogic();
        // ウェイト用インスタンス
        WaitLogic waitLogic = new WaitLogic();
        // メッセージテキストを1行ごとに分割して文字列リストに追加
        // 外枠上線
        frameLogic.drawUpBorder(text);

            // 文字列を一文字ずつ全角文字に変換
            char[] chars = messageLogic.convert(text);
            // 外枠左線
            frameLogic.drawLeftBorder();
            // 空白以外の文字列を1文字ずつウェイトをかけて表示
            for (char c : chars) {

                System.out.print(c);
                if (c != '　') {
                    waitLogic.wait(waitNum);
                }
            }
            // 外枠右線
            frameLogic.drawRightBorder();

        waitLogic.wait(3);
        // 外枠下線
        frameLogic.drawBottomBorder(text);
        System.out.println("\n");
    }


}
