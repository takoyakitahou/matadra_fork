package mataDra.logics.ui;

/**
 * 時間操作に関するクラス
 *
 * @author ken4310
 *
 */
public class WaitLogic {

    /**
     * 特定の時間メッセージを一時停止する
     *
     * @param num
     */
    public void wait(int num) {
        // 一時停止するミリ秒数
        int millis = 0;
        // 引数numの値でsleepする時間を変更
        switch (num) {
        case 1:
            millis = 10;
            break;
        case 2:
            millis = 25;
            break;
        case 3:
            millis = 100;
            break;
        case 4:
            millis = 250;
            break;
        case 5:
            millis = 500;
            break;
        case 6:
            millis = 750;
            break;
        case 7:
            millis = 1000;
            break;
        case 8:
            millis = 1500;
            break;
        default:
            millis = 0;
            break;
        }
        // ミリ秒がゼロより大きい場合、sleepによる一時停止処理を実行
        if (millis > 0) {
            // 例外処理を自動作成
            try {
                Thread.sleep(millis);
            } catch (InterruptedException e) {
                // TODO 自動生成された catch ブロック
                e.printStackTrace();
            }
        } else {
            // 一時停止しない
        }
    }
}
