package mataDra.dao.images;

import java.util.List;

import mataDra.dao.DAO;
import mataDra.entity.images.EnemyImageEntity;

public class EnemyImageDAO extends DAO<EnemyImageEntity>{

    @Override
    public List<EnemyImageEntity> registerAll() {
        // TODO 自動生成されたメソッド・スタブ

        setEntity(new EnemyImageEntity(0, "おばけきのこ", "茸", "化"));
        setEntity(new EnemyImageEntity(1, "ポイズンマタンゴ", "茸", "毒"));
        setEntity(new EnemyImageEntity(2, "マージマタンゴ", "茸", "魔"));
        setEntity(new EnemyImageEntity(3, "キングマタンゴ", "茸", "王"));
        setEntity(new EnemyImageEntity(4, "りゅうおう","王", "竜"));
        return getEntity();
    }

}
