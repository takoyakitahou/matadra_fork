package mataDra.main.test;

import mataDra.view.ui.MessageArea;

public class MessageTest {

    public static void main(String[] args) throws InterruptedException {
        String text = "ABCDEFG\n123456789\nあいうえおかきくけこさしすせそたちつてとなにぬねのはひふへほまみむめもらりるれろがぎぐげご";
        MessageArea ma = new MessageArea();
        ma.show(text, 1);
        ma.show(text, 2);
        ma.show(text, 3);

    }

}
