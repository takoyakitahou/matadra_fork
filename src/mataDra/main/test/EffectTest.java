package mataDra.main.test;

import java.util.Random;

import mataDra.logics.images.EffectImageLogic;
import mataDra.view.ui.EffectImageArea;

public class EffectTest {
	public static void main(String[] args) {

		EffectImageLogic effectImageLogic = new EffectImageLogic();
		EffectImageArea effectImageArea = new EffectImageArea();
		int index = 5;
		int count = new Random().nextInt(5);

		// インデックスをすべて再生(単体攻撃)
		for (int i = 0; i < index; i++) {
			int target = new Random().nextInt(count);
			String text = effectImageLogic.exec(i, count, target);
			effectImageArea.show(text, 2);

		}
		// インデックスをすべて再生(全体攻撃)
		for (int i = 0; i < index; i++) {
			String text = effectImageLogic.exec(i, count);
			effectImageArea.show(text, 2);

		}



	}

}
