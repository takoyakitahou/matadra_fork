package mataDra.main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import mataDra.dao.creatures.PlayerDAO;
import mataDra.entity.creatures.PlayerEntity;
import mataDra.logics.images.EffectImageLogic;
import mataDra.logics.sounds.SoundLogic;
import mataDra.logics.ui.BorderFrameLogic;
import mataDra.logics.ui.FrameLogic;
import mataDra.logics.ui.MeshFrameLogic;
import mataDra.logics.ui.StrongFrameLogic;
import mataDra.logics.ui.WaitLogic;
import mataDra.view.scenes.Making;
import mataDra.view.ui.EffectImageArea;
import mataDra.view.ui.EnemyImageArea;
import mataDra.view.ui.FieldImageArea;
import mataDra.view.ui.FrameArea;
import mataDra.view.ui.MessageArea;
import mataDra.view.ui.SoundArea;

public class Demo {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ
		MessageArea ma = new MessageArea();
		WaitLogic waitLogic = new WaitLogic();

		List<FrameLogic> frames = new ArrayList<>();
		frames.add(new MeshFrameLogic());
		frames.add(new BorderFrameLogic());
		frames.add(new StrongFrameLogic());

		FrameArea fa = new FrameArea();
		FieldImageArea fieldImageArea = new FieldImageArea();
		String text;

		text ="MATANGO　AND　DRAGONS";
		fa.show(text, 0, frames.get(1));
		Making making = new Making();

		PlayerDAO dao = new PlayerDAO();
		PlayerEntity player = dao.findByIndex(0);
		making.exec(player);
		waitLogic.wait(5);
		text ="国王の間";
		fa.show(text, 0, frames.get(1));

		text ="よくきたなnameよ。\nこの世界は闇の王、キングマタンゴの驚異にさられておる。\n頼むnameよ。キングマタンゴを倒してこの世界に光を取り戻してくれ!";
		text= text.replaceAll("name", player.getName());
		ma.show(text, 2);

		text = "1：はい／2：いいえ";
		fa.show(text, 0, frames.get(1));
		int select = new Scanner(System.in).nextInt();
		if(select>=2){
			text="突然の死";
			fa.show(text, 0, frames.get(2));
			waitLogic.wait(8);
			text = "GAME　OVER";
			fa.show(text, 0, frames.get(1));
			return;

		}


		waitLogic.wait(8);
		text="そして、勇者nameの冒険が始まった";
		text= text.replaceAll("name", player.getName());
		fa.show(text, 0, frames.get(1));

		waitLogic.wait(8);
		text="広大なフィールドを大冒険!";
		fa.show(text, 0, frames.get(2));
		waitLogic.wait(8);
		fieldImageArea.show(1);
		text="名前："+player.getName()+"／HP："+player.getHp()+"／Level："+player.getLevel();
		fa.show(text, 0, frames.get(1));


		waitLogic.wait(8);
		text="大迫力の戦闘シーン!";
		fa.show(text, 0, frames.get(2));
		waitLogic.wait(8);
		List<Integer> groupIndex = new ArrayList<>();
		groupIndex.addAll(Arrays.asList(1,2,3,0,0));
		EnemyImageArea enemyImageArea = new EnemyImageArea();
		enemyImageArea.show(groupIndex);

		EffectImageLogic effectImageLogic = new EffectImageLogic();
		EffectImageArea effectImageArea = new EffectImageArea();

		text = effectImageLogic.exec(0, 5, 3);
		effectImageArea.show(text, 2);

		SoundLogic soundLogic = new SoundLogic();
		SoundArea soundArea = new SoundArea();
		soundArea.show(soundLogic.exec(0), 1);

		waitLogic.wait(8);
		text="仲間を集めて敵に挑め!";
		fa.show(text, 0, frames.get(2));

		waitLogic.wait(8);
		text="なんとガチャが無料で遊べちまうんだ";
		fa.show(text, 0, frames.get(2));

		waitLogic.wait(8);
		text="超大作コンソールRPG";
		fa.show(text, 0, frames.get(2));

		waitLogic.wait(8);
		text ="MATANGO　AND　DRAGONS\n\nCOMING　SOON・・・";
		ma.show(text, 2);
	}

}
